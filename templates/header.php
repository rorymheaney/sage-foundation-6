<header class="header header--main">
    <div class="header__title-bar" data-responsive-toggle="top-menu" data-hide-for="medium">
        <button class="menu-icon" type="button" data-toggle></button>
        <div class="header__title-bar-title">Menu</div>
    </div>
    <div class="header__top-bar" id="top-menu">
        <div class="header__top-bar-left">
            <ul class="menu header__menu">
                <li class="home">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        
                        <img src="<?= get_template_directory_uri(); ?>/dist/images/everlywed-logo.png" alt="<?php bloginfo('name'); ?>">

                    </a>
                </li>
            </ul>
        </div>
        <div class="header__top-bar-right">
            <ul class="header__dropdown-menu" data-dropdown-menu>
                <?php if (has_nav_menu('primary_navigation')) :?>
                    <?php wp_nav_menu([
                        'theme_location' => 'primary_navigation', 
                        'menu_class' => 'nav', 
                        'container' => '', 
                        'items_wrap' => '%3$s', 
                        'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu(),
                        'link_before' => '<span class="header__menu-link-span">',     
                        'link_after'  => '</span>'
                    ]);?>
                <?php endif;?>
            </ul>
        </div>
    </div>
</header>
